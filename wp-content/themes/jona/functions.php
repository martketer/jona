<?php

if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
		echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php') ) . '</a></p></div>';
	});

	add_filter('template_include', function($template) {
		return get_stylesheet_directory() . '/dist/no-timber.html';
	});

	return;
}

// Register Menus
function register_menus() {
  register_nav_menus(
    array(
      'header-menu' => __( 'Header Menu' )
    )
  );
}
add_action( 'init', 'register_menus' );

Timber::$dirname = array('templates', 'views');

class LumberSite extends TimberSite {

	function __construct() {
		add_theme_support( 'post-formats' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'get_twig', array( $this, 'add_to_twig' ) );
		add_action( 'init', array( $this, 'register_post_types' ) );
		add_action( 'init', array( $this, 'register_taxonomies' ) );
		parent::__construct();
	}

	function register_post_types() {
			register_post_type( 'products',
				array(
					'labels' => array(
						'name' => __( 'Products' ),
						'singular_name' => __( 'Product' ),
						'edit_item' => __('Edit Product'),
						'add_new_item'       => __( 'Add New Product'),
					),
					'public' => true,
					'has_archive' => false,
					'menu_position' => 3,
					'hierarchical' => true,
					'show_in_rest' => true,
					'supports' => array(
						'title', 'page-attributes'
					),
					'menu_icon' => 'dashicons-screenoptions',
					'rewrite' => array('slug' => 'product'),
				)
			);
		register_post_type( 'team',
			array(
				'labels' => array(
					'name' => __( 'Team' ),
					'singular_name' => __( 'Team Member' ),
					'edit_item' => __('Edit Team Member'),
					'add_new_item'       => __( 'Add New Team Member'),
				),
				'show_ui' => true,
				'has_archive' => false,
				'menu_position' => 2,
				'hierarchical' => false,
				'show_in_rest' => true,
				'supports' => array(
					'title', 'page-attributes'
				),
				'menu_icon' => 'dashicons-groups',
			)
		);
	}

	function register_taxonomies() {
		//this is where you can register custom taxonomies
	}

	function add_to_context( $context ) {
		$context['menu'] = new TimberMenu('header-menu');
		$context['site'] = $this;
		$args = array(
		  'post_type' => 'products',
			'orderby' => 'menu_order',
			'order' => 'ASC'
		);
		$context['products'] = Timber::get_posts($args);
		$context['cdnUrl'] = '//cdn.lumber.dev';
		$context['baseUrl'] = '//lumber.dev';
		return $context;
	}

	function myfoo( $text ) {
		$text .= ' bar!';
		return $text;
	}

	function add_to_twig( $twig ) {
		/* this is where you can add your own functions to twig */
		$twig->addExtension( new Twig_Extension_StringLoader() );
		$twig->addFilter('myfoo', new Twig_SimpleFilter('myfoo', array($this, 'myfoo')));
		return $twig;
	}

}

new LumberSite();

function blog_query( $query ) {
  if ( $query->is_main_query() && !is_admin() && is_home()) {
    $query->set( 'post_type', array( 'post' ));
		$query->set( 'posts_per_page', 3);
  }
}
add_action( 'pre_get_posts', 'blog_query' );
