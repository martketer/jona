<?php
/**
 * The Template for displaying all single posts
 *
 *
 * @package  WordPress
 * @subpackage  Timber
 */

$args = array(
'posts_per_page'   => 5,
'orderby'          => 'date',
'order'            => 'DESC',
'post_type'        => 'post',
);

$context['archives'] = new TimberArchives( $args );

Timber::render( array( 'sidebar.twig' ), $context );
